# Copyright 2013, Michael Stahn
# Use of this source code is governed by a GPLv2-style license that can be
# found in the LICENSE file.
"""Protocol Independent Multicast."""

from pypacker import pypacker, checksum
from pypacker.pypacker import FIELD_FLAG_AUTOUPDATE


class PIM(pypacker.Packet):
	__hdr__ = (
		("v_type", "B", 0x20),
		("rsvd", "B", 0),
		("sum", "H", 0, FIELD_FLAG_AUTOUPDATE)  # _sum = sum
	)

	def _get_v(self):
		return self.v_type >> 4

	def _set_v(self, v):
		self.v_type = (v << 4) | (self.v_type & 0xF)
	v = property(_get_v, _set_v)

	def _get_type(self):
		return self.v_type & 0xF

	def _set_type(self, pimtype):
		self.v_type = (self.v_type & 0xF0) | pimtype
	type = property(_get_type, _set_type)

	def _update_fields(self):
		if self.sum_au_active and self._changed():
			self.sum = 0
			self.sum = checksum.in_cksum(pypacker.Packet.bin(self))
