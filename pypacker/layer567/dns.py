# Copyright 2013, Michael Stahn
# Use of this source code is governed by a GPLv2-style license that can be
# found in the LICENSE file.
"""Domain Name System."""
import logging

from pypacker import pypacker, triggerlist
from pypacker.pypacker import FIELD_FLAG_AUTOUPDATE
from pypacker.structcbs import unpack_H, unpack_HHHH

logger = logging.getLogger("pypacker")


DNS_Q			= 0
DNS_R			= 1

# Opcodes
DNS_QUERY		= 0
DNS_IQUERY		= 1
DNS_STATUS		= 2
DNS_NOTIFY		= 4
DNS_UPDATE		= 5

# Flags
DNS_AN			= 0x8000		# this is a response
DNS_CD			= 0x0010		# checking disabled
DNS_AD			= 0x0020		# authenticated data
DNS_Z			= 0x0040		# unused
DNS_RA			= 0x0080		# recursion available
DNS_RD			= 0x0100		# recursion desired
DNS_TC			= 0x0200		# truncated
DNS_AA			= 0x0400		# authoritative answer

# Response codes
DNS_RCODE_NOERR		= 0
DNS_RCODE_FORMERR	= 1
DNS_RCODE_SERVFAIL	= 2
DNS_RCODE_NXDOMAIN	= 3
DNS_RCODE_NOTIMP	= 4
DNS_RCODE_REFUSED	= 5
DNS_RCODE_YXDOMAIN	= 6
DNS_RCODE_YXRRSET	= 7
DNS_RCODE_NXRRSET	= 8
DNS_RCODE_NOTAUTH	= 9
DNS_RCODE_NOTZONE	= 10

# RR types
DNS_TYPE_A		= 1
DNS_TYPE_NS		= 2
DNS_TYPE_CNAME		= 5
DNS_TYPE_SOA		= 6
DNS_TYPE_WKS		= 11
DNS_TYPE_PTR		= 12
DNS_TYPE_HINFO		= 13
DNS_TYPE_MINFO		= 14
DNS_TYPE_MX		= 15
DNS_TYPE_TXT		= 16
DNS_TYPE_RP		= 17
DNS_TYPE_SIG		= 24
DNS_TYPE_GPOS		= 27
DNS_TYPE_AAAA		= 28
DNS_TYPE_LOC		= 29
DNS_TYPE_SRV		= 33
DNS_TYPE_NAPTR		= 35
DNS_TYPE_KX		= 36
DNS_TYPE_CERT		= 37
DNS_TYPE_DNAME		= 39
DNS_TYPE_DS		= 43
DNS_TYPE_SSHFP		= 44
DNS_TYPE_IPSECKE	= 45
DNS_TYPE_RRSIG		= 46
DNS_TYPE_NSEC		= 47
DNS_TYPE_DNSKEY		= 48
DNS_TYPE_DHCID		= 49
DNS_TYPE_NSEC3		= 50
DNS_TYPE_NSEC3PARAM	= 51
DNS_TYPE_TLSA		= 52
DNS_TYPE_SPF		= 99
DNS_TYPE_TKEY		= 249
DNS_TYPE_TSIG		= 250
DNS_TYPE_IXFR		= 251
DNS_TYPE_AXFR		= 252
DNS_TYPE_CAA		= 257
DNS_TYPE_TA		= 32768
DNS_TYPE_DLV		= 32769


# RR classes
DNS_CLASS_IN		= 1
DNS_CLASS_CHAOS		= 3
DNS_CLASS_HESIOD	= 4
DNS_CLASS_ANY		= 255


def get_bts_for_msg_compression(tl_packet):
	"""return -- header bytes of DNS or b"" """
	# DNS.Triggestlist[sub] -> sub._triggelistpacket_parent == DNS
	if tl_packet._triggelistpacket_parent is not None:
		#logger.debug("Returning parent header bytes")
		return tl_packet._triggelistpacket_parent.header_bytes
	#logger.debug("No parent header bytes :/")
	return b""


class DNS(pypacker.Packet):
	__hdr__ = (
		("id", "H", 0x1234),
		("flags", "H", DNS_AD | DNS_RD),
		("questions_amount", "H", 0, FIELD_FLAG_AUTOUPDATE),
		("answers_amount", "H", 0, FIELD_FLAG_AUTOUPDATE),
		("authrr_amount", "H", 0, FIELD_FLAG_AUTOUPDATE),
		("addrr_amount", "H", 0, FIELD_FLAG_AUTOUPDATE),
		("queries", None, triggerlist.TriggerList),
		("answers", None, triggerlist.TriggerList),
		("auths", None, triggerlist.TriggerList),
		("addrecords", None, triggerlist.TriggerList)
	)

	class Query(pypacker.Packet):
		"""DNS question."""
		__hdr__ = (
			("name", None, b"\x03www\x04test\x03com\x00"),
			("type", "H", DNS_TYPE_A),
			("quest_clz", "H", DNS_CLASS_IN)
		)

		name_s = pypacker.get_property_dnsname("name", cb_mc_bytes=get_bts_for_msg_compression)
		type_t = pypacker.get_property_translator("type", "DNS_TYPE_")

		def _get_clz(self):
			return self.quest_clz & 0b01111111

		def _set_clz(self, value):
			self.quest_clz = (self.quest_clz & 0b10000000) | (value & 0b01111111)

		clz = property(_get_clz, _set_clz)
		clz_t = pypacker.get_property_translator("clz", "DNS_CLASS_")

		def compress(self, ref_bts):
			name_compressed = pypacker.compress_dns(self.name, ref_bts)

			if name_compressed is not None:
				self.name = name_compressed

		def _dissect(self, buf):
			q_end = DNS.get_dns_length(buf)
			self.name = buf[:q_end]
			#logger.debug("Query name=%r" % buf[:q_end].tobytes())
			return len(buf)  # name (including 0) + type + clz

	class _AnswerBase(pypacker.Packet):
		"""DNS resource record."""
		__pre_hdr__ = (
			("name", None, b"\xc0\x0c"),
			("type", "H", DNS_TYPE_A),
			("flush_clz", "H", DNS_CLASS_IN), # Bits: x y*14
			("ttl", "I", 180),
			("dlen", "H", 4)
		)

		name_s = pypacker.get_property_dnsname("name", cb_mc_bytes=get_bts_for_msg_compression)
		type_t = pypacker.get_property_translator("type", "DNS_TYPE_")

		def _get_clz(self):
			return self.flush_clz & 0b01111111

		def _set_clz(self, value):
			self.flush_clz = (self.flush_clz & 0b10000000) | (value & 0b01111111)

		clz = property(_get_clz, _set_clz)
		clz_t = pypacker.get_property_translator("clz", "DNS_CLASS_")

		def _dissect(self, buf, extract=None):
			namelen = DNS.get_dns_length(buf)
			self.name = buf[:namelen]
			# Needed to set format
			dlen_start = namelen + 8
			dlen = unpack_H(buf[dlen_start: dlen_start + 2])[0]
			# Only for Answer, redundant for others
			self.address = buf[dlen_start + 2: dlen_start + 2 + dlen]
			#logger.debug("address: %s" % self.address)
			if extract is not None:
				extract["namelen"] = namelen
				extract["dlen"] = dlen
			return dlen_start + 2

		def compress(self, ref_bts):
			name_compressed = pypacker.compress_dns(self.name, ref_bts)

			if name_compressed is not None:
				self.name = name_compressed

	class Answer(_AnswerBase):
		__hdr__ = ()

	class AnswerName(_AnswerBase):
		"""TODO: make generic for: CNAME, PTR"""
		__hdr__ = (
			("namesub", None, b"1234"),
		)

		def _dissect(self, buf):
			extract = {}
			len_parent = super()._dissect(buf, extract=extract)
			#logger.debug("namesub: %r" % buf[len_parent:].tobytes())
			#namesub_len = DNS.get_dns_length(buf[len_parent:])
			self.namesub = buf[len_parent: len_parent + extract["dlen"]]
			return len_parent + extract["dlen"]

		namesub_s = pypacker.get_property_dnsname("namesub", cb_mc_bytes=get_bts_for_msg_compression)

		def _update_fields(self):
			self.dlen = len(self.namesub)

	"""
	class AnswerPtr(_AnswerBase):
		__hdr__ = (
			("name", None, b"some.domain"),
		)

		def _dissect(self, buf):
			extract = {}
			len_parent = super()._dissect(buf, extract=extract)
			#logger.debug("CNAME: %r" % buf[len_parent:].tobytes())
			self.cname = buf[len_parent:]
			return len(buf)

		cname_s = pypacker.get_property_dnsname("cname", cb_mc_bytes=get_bts_for_msg_compression)

		def _update_fields(self):
			self.dlen = len(self.cname)
	"""

	class AnswerA(_AnswerBase):
		__hdr__ = (
			("address", None, b"1234"),
		)

		def _dissect(self, buf):
			#extract = {}
			len_parent = super()._dissect(buf)
			#self.cname = buf[-extract["dlen"]: ]
			#logger.debug("AnswerA address: %r" % buf[len_parent: ].tobytes())
			return len_parent + 4

		address_s = pypacker.get_property_ip4("address")

		def _update_fields(self):
			self.dlen = len(self.address)

	class AnswerDS(_AnswerBase):
		__hdr__ = (
			("key", "H", 0),
			("algo", "B", 0),
			("digest", "B", 0)
		)

		def _dissect(self, buf):
			extract = {}
			len_parent = super()._dissect(buf, extract=extract)
			#self.cname = buf[-extract["dlen"]: ]
			return len_parent + 4

	class DNSKey(_AnswerBase):
		__hdr__ = (
			("flags", "H", 0),
			("proto", "B", 0),
			("algo", "B", 0),
			("pkey", None, b""),
		)

		def _dissect(self, buf):
			extract = {}
			#logger.debug("Dissecting via parent")
			len_parent = super()._dissect(buf, extract=extract)
			#logger.debug("len_parent=%r, extract=%r" % (len_parent, extract))
			pkey_bts = buf[len_parent + 4:]
			#logger.debug("pkey len=%d, pkey=%r" % (len_parent - (extract["namelen"] + 10 + 4), pkey_bts.tobytes()) )
			self.pkey = pkey_bts
			return len(buf)

		def _update_fields(self):
			self.dlen = 4 + len(self.pkey)

	class RRSIG(_AnswerBase):
		__hdr__ = (
			("typecov", "H", 0),
			("algo", "B", 0),
			("labels", "B", 0),
			("origttl", "I", 0),
			("sigexp", "I", 0),
			("sigincept", "I", 0),
			("keytag", "H", 0),
			("signame", None, b""),
			("signature", None, b""),
		)

		signame_s = pypacker.get_property_dnsname("signame", cb_mc_bytes=get_bts_for_msg_compression)

		def _dissect(self, buf):
			extract = {}
			#logger.debug("Dissecting via parent")
			len_parent = super()._dissect(buf, extract=extract)
			#logger.debug("len_parent=%r, extract=%r" % (len_parent, extract))

			signame_start = len_parent + 18
			signame_len = DNS.get_dns_length(buf[signame_start:])

			self.signame = buf[signame_start: signame_start + signame_len]
			self.signature = buf[signame_start + signame_len:]
			#logger.debug("signame_bts=%r, signature_bts=%r" % (signame_bts.tobytes(), signature_bts.tobytes()))

			return len(buf)

		def _update_fields(self):
			self.dlen = 18 + len(self.signame) + len(self.signature)

	class _AuthBase(pypacker.Packet):
		__pre_hdr__ = (
			("name", None, b"\xc0\x0c"),
			("type", "H", DNS_TYPE_A),
			("clz", "H", DNS_CLASS_IN),
			("ttl", "I", 0),
			("dlen", "H", 0), # length of the rest of header: server + x, x becmoes body content
		)

		name_s = pypacker.get_property_dnsname("name", cb_mc_bytes=get_bts_for_msg_compression)
		type_t = pypacker.get_property_translator("type", "DNS_TYPE_")
		clz_t = pypacker.get_property_translator("type", "DNS_CLASS_")

		def _dissect(self, buf, extract=None):
			name_end = DNS.get_dns_length(buf)
			#logger.debug("DNS Auth in %r, name len=%r, value=%r" % (self.__class__, name_end, buf[:name_end].tobytes()))
			self.name = buf[:name_end]
			dlen_start = name_end + 8
			dlen = unpack_H(buf[dlen_start: dlen_start + 2])[0]
			if extract is not None:
				extract["dlen"] = dlen
			return dlen_start + 2

		def compress(self, ref_bts):
			name_compressed = pypacker.compress_dns(self.name, ref_bts)

			if name_compressed is not None:
				self.server = name_compressed

	# TODO: add more compression

	class Auth(_AuthBase):
		"""Auth, generic type."""
		__hdr__ = ()

		def _update_fields(self):
			self.dlen = len(self.body_bytes)

	class AuthNS(_AuthBase):
		__hdr__ = (
			("server", None, b"\x03www\x04test\x03com\x00"),
		)

		server_s = pypacker.get_property_dnsname("server", cb_mc_bytes=get_bts_for_msg_compression)

		def _dissect(self, buf):
			extract = {}
			parent_len = super()._dissect(buf, extract=extract)
			self.server = buf[-extract["dlen"]:]
			#logger.debug("server=%r, len=%r" % ( buf[-extract["dlen"]: ].tobytes(), extract["dlen"]))
			return parent_len + extract["dlen"]

		def _update_fields(self):
			self.dlen = len(self.server)

		def compress(self, ref_bts):
			server_compressed = pypacker.compress_dns(self.server, ref_bts)

			if server_compressed is not None:
				self.server = server_compressed

	class AuthSOA(_AuthBase):
		__hdr__ = (
			("priname", None, b"\x03www\x04test\x03com\x00"),
			("mailbox", None, b"\x03www\x04test\x03com\x00"),
			("serial", "H", 0),
			("refresh", "H", 0),
			("retry", "H", 0),
			("expire", "H", 0),
			("minttl", "H", 0)
		)

		priname_s = pypacker.get_property_dnsname("priname", cb_mc_bytes=get_bts_for_msg_compression)
		mailbox_s = pypacker.get_property_dnsname("mailbox", cb_mc_bytes=get_bts_for_msg_compression)

		def _dissect(self, buf):
			extract = {}
			parent_len = super()._dissect(buf, extract=extract)
			priname_len = DNS.get_dns_length(buf[parent_len:])
			mailbox_len = DNS.get_dns_length(buf[parent_len + priname_len:])
			#logger.debug("priname_len=%d=%r, priname_len=%d=%r", priname_len, buf[parent_len:parent_len+priname_len].tobytes(),
			#	mailbox_len,  buf[parent_len+priname_len:parent_len+priname_len+mailbox_len])
			self.priname = buf[parent_len: parent_len + priname_len]
			self.mailbox = buf[parent_len + priname_len: parent_len + priname_len + mailbox_len]
			return parent_len + priname_len + mailbox_len + 10

		def _update_fields(self):
			self.dlen = len(self.name) + 10 + len(self.priname) + len(self.mailbox) + 10

	class AddRecord(pypacker.Packet):
		"""DNS additional records."""
		# TODO; many types, see mdns.pcap, 94
		__hdr__ = (
			("name", None, b"\xc0\x0c"),
			("type", "H", DNS_TYPE_A),
			("cache_clz", "H", DNS_CLASS_IN),
			("ttl", "I", 0),
			("dlen", "H", 0),
			#("address", None, b"\x01\x02\x03\x04")
		)

		name_s = pypacker.get_property_dnsname("name", cb_mc_bytes=get_bts_for_msg_compression)
		type_t = pypacker.get_property_translator("type", "DNS_TYPE_")

		def _get_clz(self):
			return self.flush_clz & 0b01111111

		def _set_clz(self, value):
			self.cache_clz = (self.cache_clz & 0b10000000) | (value & 0b01111111)

		clz = property(_get_clz, _set_clz)
		clz_t = pypacker.get_property_translator("clz", "DNS_CLASS_")

		"""
		def _get_address_readable(self):
			if self.type == DNS_TYPE_A:
				return pypacker.ip4_bytes_to_str(self.address)
			if self.type == DNS_TYPE_AAAA:
				return pypacker.ip6_bytes_to_str(self.address)
			if self.type == DNS_TYPE_CNAME:
				return pypacker.dns_name_decode(self.address, cb_mc_bytes=lambda: get_bts_for_msg_compression(self))
			# Unknown type
			return ""

		def _set_address_readable(self, address):
			if self.type == DNS_TYPE_A:
				self.address = pypacker.ip4_str_to_bytes(address)
			elif self.type == DNS_TYPE_AAAA:
				self.address = pypacker.ip6_str_to_bytes(address)
			elif self.type == DNS_TYPE_CNAME:
				self.address = pypacker.dns_name_encode(address)

		# Format depends on on type
		address_s = property(_get_address_readable, _set_address_readable)
		"""

		def _dissect(self, buf):
			# Needed to set format
			name_end = DNS.get_dns_length(buf)
			self.name = buf[:name_end]
			dlen_start = name_end + 8
			#dlen = unpack_H(buf[dlen_start: dlen_start + 2])[0]
			#self.address = buf[dlen_start + 2: dlen_start + 2 + dlen]
			#logger.debug("Address len: %r" % dlen)
			#return dlen_start + 2 + dlen
			return dlen_start + 2

		def compress(self, ref_bts):
			name_compressed = pypacker.compress_dns(self.name, ref_bts)

			if name_compressed is not None:
				self.name = name_compressed

	class AddRecordRoot(pypacker.Packet):
		__hdr__ = (
			("name", None, b"\x00"), # 0=root
			("type", "H", 0),
			("pld_size", "H", 0),
			("ext_rcode", "B", 0),
			("ednvers", "B", 0),
			("z", "H", 0),
			("dlen", "H", 0)
		)

	@staticmethod
	def get_dns_length(bts):
		"""
		return -- Length of DNS name including terminating 0 (if present)
		"""
		off = 0

		while off < len(bts):
			# Check for pointer
			if bts[off] & 0xC0:
				#logger.debug("Found pointer at %d", off)
				return off + 2
			# Found terminating 0
			if bts[off] == 0x00:
				#logger.debug("Found 0 byte at %d", off)
				return off + 1
			off += bts[off] + 1

		return 0

	@staticmethod
	def _dissect_queries(amount, collect_queries=True):
		amount_collect = [amount, collect_queries]

		def _dissect_queries_sub(buf):
			#logger.debug(">> Dissecting queries: %r" % amount_collect)
			queries = []
			off = 0
			while amount_collect[0] > 0 and off < len(buf):
				# Find name by 0-termination
				q_end = off + DNS.get_dns_length(buf[off:]) + 4

				if amount_collect[1]:
					q = DNS.Query(buf[off: q_end])
					queries.append(q)
				off = q_end
				amount_collect[0] -= 1
			#logger.debug("Returning from _dissect_queries_sub")
			return queries if amount_collect[1] else off
		return _dissect_queries_sub

	@staticmethod
	def _dissect_answers(amount, collect_answers=True):
		amount_collect = [amount, collect_answers]
		type__answerclass = {
			DNS_TYPE_A: DNS.AnswerA,
			DNS_TYPE_CNAME: DNS.AnswerName,
			DNS_TYPE_DS: DNS.AnswerDS,
			DNS_TYPE_DNSKEY: DNS.DNSKey,
			DNS_TYPE_RRSIG: DNS.RRSIG
		}

		def _dissect_answers_sub(buf):
			#logger.debug(">> Dissecting answers: %r" % amount_collect)
			answers = []
			off = 0

			while amount_collect[0] > 0 and off < len(buf):
				#logger.debug("Dissecting answer No. %d" % amount_collect[0])
				# Find name by label/0-termination
				# DNS name:x + type:2 + class:2 + ttl:4
				off_type = off + DNS.get_dns_length(buf[off:])
				off_dlen = off_type + 2 + 2 + 4
				dlen = unpack_H(buf[off_dlen: off_dlen + 2])[0]
				a_end = off_dlen + 2 + dlen

				if amount_collect[1]:
					atype = unpack_H(buf[off_type: off_type + 2])[0]
					a = type__answerclass.get(atype, DNS.Answer)(buf[off: a_end])
					#logger.debug("Bytes used=%r, type=%r, clz=%r" % (buf[off: a_end].tobytes(), atype, a.__class__))
					answers.append(a)
				off = a_end
				amount_collect[0] -= 1
			return answers if amount_collect[1] else off
		return _dissect_answers_sub

	@staticmethod
	def _dissect_authserver(amount, collect_authserver=True):
		amount_collect = [amount, collect_authserver]
		authtype__clz = {DNS_TYPE_NS: DNS.AuthNS, DNS_TYPE_SOA: DNS.AuthSOA}

		def _dissect_authserver_sub(buf):
			#logger.debug(">> Dissecting authserver: %r" % amount_collect)
			authserver = []
			off = 0

			while amount_collect[0] > 0 and off < len(buf):
				#logger.debug("Dissecting auth No. %d" % amount_collect[0])
				#logger.debug("off=%d, buf=%r" % (off, buf[:15].tobytes()))
				# DNS name:x + type:2 + class:2 + ttl:4
				off_type = off + DNS.get_dns_length(buf[off:])
				dlen_start = off_type + 2 + 2 + 4
				#logger.debug("a_end=%d" % a_end)
				dlen = unpack_H(buf[dlen_start: dlen_start + 2])[0]
				#logger.debug("dlen=%d, bts=%r" % (dlen, buf[a_end: a_end + 2].tobytes()))
				a_end = dlen_start + 2 + dlen

				if amount_collect[1]:
					atype = unpack_H(buf[off_type: off_type + 2])[0]
					a = authtype__clz.get(atype, DNS.Auth)(buf[off: a_end])
					#logger.debug("Bytes used=%r, type=%r, clz=%r" % (buf[off: a_end].tobytes(), atype, a.__class__))
					authserver.append(a)
				off = a_end
				amount_collect[0] -= 1
			return authserver if amount_collect[1] else off
		return _dissect_authserver_sub

	@staticmethod
	def _dissect_addreq(amount, collect_addreq=True):
		amount_collect = [amount, collect_addreq]

		def _dissect_addreq_sub(buf):
			#logger.debug(">> Dissecting addreq: %r" % buf.tobytes())
			addrecords = []
			off = 0

			while amount_collect[0] > 0 and off < len(buf):
				if buf[off: off + 3] == b"\x00\x00\x29":
					if amount_collect[1]:
						a = DNS.AddRecordRoot(buf[off: off + 11])
						addrecords.append(a)
					off += 11
				else:
					name_len = DNS.get_dns_length(buf[off:])
					dlen = unpack_H(buf[off + name_len + 8: off + name_len + 8 + 2])[0]
					#logger.debug("AddRecord dlen: %r" % dlen)
					a_end = off + name_len + 10 + dlen

					if amount_collect[1]:
						a = DNS.AddRecord(buf[off: a_end])
						addrecords.append(a)
					off = a_end

				amount_collect[0] -= 1
			return addrecords if amount_collect[1] else off
		return _dissect_addreq_sub

	def _dissect(self, buf):
		#logger.debug("Dissect start")
		# Unpack basic data to get things done
		quests_amount, ans_amount, authserver_amount, addreq_amount = unpack_HHHH(buf[4: 12])
		#logger.debug("quests_amount=%d, ans_amount=%d, authserver_amount=%d, addreq_amount=%d" % (
		#	quests_amount, ans_amount, authserver_amount, addreq_amount))
		# Sanity check: assume max amount of 50 addresses
		if quests_amount > 50 or ans_amount > 50 or authserver_amount > 50 or addreq_amount > 50:
			raise Exception("Address count too high, invalid packet")
		off = 12
		#logger.debug(">> Dissecting %d queries: off=%d" % (quests_amount, off))
		addlen = DNS._dissect_queries(quests_amount, collect_queries=False)(buf[off:])
		self.queries(buf[off: off + addlen], DNS._dissect_queries(quests_amount))
		off += addlen

		#logger.debug(">> Dissecting %d answers: off=%d" % (ans_amount, off))
		addlen = DNS._dissect_answers(ans_amount, collect_answers=False)(buf[off:])
		self.answers(buf[off: off + addlen], DNS._dissect_answers(ans_amount))
		off += addlen

		#logger.debug(">> Dissecting %d authservers: off=%d" % (authserver_amount, off))
		addlen = DNS._dissect_authserver(authserver_amount, collect_authserver=False)(buf[off:])
		self.auths(buf[off: off + addlen], DNS._dissect_authserver(authserver_amount))
		off += addlen

		#logger.debug(">> Dissecting %d addrec: off=%d" % (addreq_amount, off))
		addlen = DNS._dissect_addreq(addreq_amount, collect_addreq=False)(buf[off:])
		self.addrecords(buf[off: off + addlen], DNS._dissect_addreq(addreq_amount))
		#logger.debug("ADdlen was: %r" % addlen)
		off += addlen
		#logger.debug("Dissect finished, off: %d" % off)

		return off

	#TYPES_COMPRESSABLE = {Query, Answer, Auth, AddRecord}
	TYPES_COMPRESSABLE = {Query, Answer, Auth}

	def _update_fields(self):
		if self._header_value_changed:
			if self.questions_amount_au_active:
				self.questions_amount = len(self.queries)
			if self.answers_amount_au_active:
				self.answers_amount = len(self.answers)
			if self.authrr_amount_au_active:
				self.authrr_amount = len(self.auths)
			if self.addrr_amount_au_active:
				self.addrr_amount = len(self.addrecords)

	def compress(self):
		"""
		DNS compress is optional: RFC 1035 -> 3.3 Standard RRs -> "RRs may be compressed"
		"""
		# Update all fields
		self.bin()
		# Make sure references are correct
		self.decompress()
		# Start at 2nd element, avoids self-referencing of 1st to itself
		ref_bts = self.header_bytes[:12]

		for tl in [self.queries, self.answers, self.auths, self.addrecords]:
			for idx, tl_element in enumerate(tl):
				if type(tl_element) in DNS.TYPES_COMPRESSABLE:
					tl_element.compress(ref_bts)

				entry_bts = tl.entry_to_bytes(idx)
				ref_bts = ref_bts + entry_bts

	def decompress(self): # pylint: disable=too-many-branches
		"""
		Decompress field values and by this make internal/external value coherent:
		(internal value is retrieved value via .._s): dns.name_s == "name" -> dns.name == "..name..".
		Needed because: Compressing is not deterministic (may occur, not a must-have), call before compressing
		or comparison reasons (eg when testing).
		"""
		# Avoid invalid pointers: reverse header field order decompress
		# [[tl, [element, [value, ...]]]]
		for idx, tl in enumerate([self.addrecords, self.auths, self.answers, self.queries]): # pylint: disable=unused-variable
			#logger.debug("Decompress in tl No. %d" % (idx + 1))

			for idx_el, element in enumerate(tl[::-1]): # pylint: disable=unused-variable
				#logger.debug("Decompress in tl idx %d" % idx_el)

				element_type = type(element)
				#type_found = True

				"""
				if element_type == DNS.AddRecord:
					if len(element.address_s) > 0:
						element.address_s = element.address_s
				"""
				if element_type == DNS.AuthNS:
					if len(element.server_s) > 0:
						element.server_s = element.server_s
				elif element_type == DNS.AnswerA:
					if len(element.address_s) > 0:
						element.address_s = element.address_s
				elif element_type == DNS.AuthSOA:
					if len(element.priname) > 0:
						element.priname_s = element.priname_s
					if len(element.mailbox) > 0:
						element.mailbox_s = element.mailbox_s
				elif element_type == DNS.AnswerName:
					if len(element.namesub) > 0:
						element.namesub_s = element.namesub_s
				#else:
				#	type_found = False

				if element_type in ELEMENTS_DECOMPRESS_NAME:
					if len(element.name_s) > 0:
						#logger.debug("Decompressing %r" % element_type)
						element.name_s = element.name_s

				#if not type_found:
				#	logger.debug("!!! Nothing to decompress: %r" % element.__class__)

	def get_resolved_addresses(self):
		"""
		return -- {"1.2.3.4": "test.com"}
		"""
		ret = {}

		if self.answers_amount == 0 or self.questions_amount == 0:
			return ret
		"""
		if self.questions_amount > 1:
			# Only one question for now
			logger.warning("More than 1 question")
			logger.warning(self.queries)
		"""
		question_dns = self.queries[0].name_s

		for answer in self.answers:
			# Assume answer for first query
			if answer.type in [DNS_TYPE_A, DNS_TYPE_AAAA]:
				ret[answer.address_s] = question_dns

		return ret


ELEMENTS_DECOMPRESS_NAME = set([DNS.AddRecord, DNS.Auth, DNS.AuthNS, DNS.Answer, DNS.AnswerA, DNS.AnswerName,
	DNS.AnswerDS, DNS.Query, DNS.DNSKey, DNS.RRSIG])
