# Copyright 2013, Michael Stahn
# Use of this source code is governed by a GPLv2-style license that can be
# found in the LICENSE file.
import logging

logger = logging.getLogger("pypacker")
#logger.setLevel(logging.DEBUG)
logger.setLevel(logging.WARNING)


class NiceFormatter(logging.Formatter):
	#EC_GREY = "\x1b[38;20m"
	EC_GREY = "\x1b[38;5;245m"
	#EC_BLUE = "\x1b[0;34m"
	EC_YELLOW = "\x1b[33;20m"
	#EC_RED = "\x1b[31;20m"
	#EC_RED = "\x1b[38;5;196m"
	EC_RED = "\x1b[38;5;205m"
	#EC_RED = "\x1b[31;49m"
	EC_RED_BOLD = "\x1b[31;1m"
	#EC_RED_BOLD = "\x1b[31;5;196m"
	EC_RESET = "\x1b[0m"

	# date output via: %(asctime)s
	FORMATS = {
		logging.DEBUG: EC_GREY + "%(levelname)s: %(module)s -> %(funcName)s -> %(lineno)d: %(message)s" + EC_RESET,
		logging.INFO: "%(message)s",
		logging.WARNING: EC_YELLOW + "%(levelname)s: %(module)s: %(lineno)d: %(message)s" + EC_RESET,
		logging.ERROR: EC_RED + "%(levelname)s: %(module)s: %(lineno)d: %(message)s" + EC_RESET,
		logging.CRITICAL: EC_RED_BOLD + "%(levelname)s: %(levelname)s: %(message)s" + EC_RESET
	}

	def __init__(self):
		super().__init__(fmt="%(levelno)d: %(msg)s", datefmt="%Y-%m-%d %H:%M:%S", style="%")

	"""
	# TODO: better solution?
	def format(self, record):
		log_fmt = self.FORMATS.get(record.levelno, self.FORMATS["DEFAULT"])
		formatter = logging.Formatter(log_fmt)
		return formatter.format(record)
	"""

	def format(self, record):
		format_orig = self._style._fmt
		self._style._fmt = self.FORMATS.get(record.levelno, logging.INFO)
		result = logging.Formatter.format(self, record)

		self._style._fmt = format_orig

		return result


logger_formatter = NiceFormatter()

logger_streamhandler = logging.StreamHandler()
logger_streamhandler.setFormatter(logger_formatter)
logger.addHandler(logger_streamhandler)

"""
# Activate for output to file
logger_filehandler = logging.FileHandler("logs.txt")
logger_filehandler.setFormatter(logger_formatter)
logger.addHandler(logger_filehandler)
"""

"""
# Preview output
for cb in [logger.debug, logger.info, logger.warning, logger.error, logger.critical]:
	cb("Testmessage 123")
"""
