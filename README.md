<p align="center">
	<img width="105" height="176" src="./pypacker_logo_large.png">
</p>

# General information
This is Pypacker: The fastest and simplest low-level packet manipulation library for Python.
See below examples for what you can do with it.

If you want to support this project you can [![Donate with PayPal](https://www.paypalobjects.com/en_US/i/btn/btn_donate_SM.gif)](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=M6GGAXJQCUHVC&source=url) via PayPal.

## What you can do with Pypacker
- Create custom Packets via keywords or from raw bytes and read/change their data via Python: [examples/create_packets.py](./examples/create_packets.py)
- Read/write packets from/to pcap file (Support only for Wireshark/tcpdump pcap format): [examples/pcap_read_write.py](./examples/pcap_read_write.py)
- Merge multiple pcap files to one file. Tries to read corrupted pcap files and allows filtering by callback: [examples/pcap_merge.py](./examples/pcap_merge.py)
- Send/receive layer 2 packets: [examples/send_receive_l2.py](./examples/send_receive_l2.py)
- Intercept (and modificate) packets e.g. for MITM: [examples/interceptor3.py](./examples/interceptor3.py)

## Prerequisites
- Python 3.x (CPython, Pypy, Jython or whatever Interpreter)
- Optional: netifaces >=0.10.6 (for utils)
- Optional (for interceptor):
  - CPython
  - Linux based system with kernel support for NFQUEUE target. The kernel config option is at:
	- Networking Options -> Network packet filtering -> Core Netfilter -> NFQUEUE target
  - iptables (alternatively nftables)
    - NFQUEUE related rulez can be added eg "iptables -I INPUT 1 -j NFQUEUE --queue-num 0"
  - libnetfilter_queue library (see http://www.netfilter.org/projects/libnetfilter_queue)

## Installation
Some examples:
- Clone newest version
  - git clone https://gitlab.com/mike01/pypacker.git
  - cd pypacker
  - python setup.py install

pip/pypi is NOT supported anymore because of their complicated authentication mechanism, sorry.

## Usage examples and documentation
See:

- Above examples
- Examples in directory ./examples
- Gitlab Wiki: https://gitlab.com/mike01/pypacker/-/wikis/home

Protocols itself (see layerXYZ) generally don't have much documentation because those are documented
by their respective RFCs/official standards.

## Testing
Tests are executed as follows:

1) Add Pypacker directory to the PYTHONPATH.

- `cd pypacker`
- `export PYTHONPATH=$(pwd):$PYTHONPATH`

2) Execute tests

- `python tests/test_pypacker.py`

**Performance test results:**
```
Hardware: Intel CPU, 4 Cores @ 3.2 GHz
Python: CPython v3.10.13

nr = new results on this machine
rounds per test: 10000
=====================================
>>> Packet parsing (Ethernet + IP + UDP + DNS): Search UDP port
Time diff: 0.3220548629760742s
nr = 31050 p/s
>>> Packet parsing (Ethernet + IP + TCP + HTTP): Search TCP port
Time diff: 0.5902166366577148s
nr = 16942 p/s
>>> Packet parsing (Ethernet + IP + TCP + HTTP): Reading all header
Time diff: 0.9773461818695068s
nr = 10231 p/s
>>> Parsing first layer (IP + ICMP)
Time diff: 0.04322528839111328s
nr = 231346 p/s
>>> Creating/direct assigning (IP only header)
Time diff: 0.06982040405273438s
nr = 143224 p/s
>>> bin() without change (IP)
Time diff: 0.02322697639465332s
nr = 430533 p/s
>>> Output with change/checksum recalculation (IP)
Time diff: 0.18998193740844727s
nr = 52636 p/s
>>> Basic/first layer parsing (Ethernet + IP + TCP + HTTP)
Time diff: 0.048027992248535156s
nr = 208211 p/s
>>> Changing Triggerlist element value (Ethernet + IP + TCP + HTTP)
Time diff: 0.04828596115112305s
nr = 207099 p/s
>>> Changing dynamic field (Ethernet + IP + TCP + HTTP)
Time diff: 0.016223669052124023s
nr = 616383 p/s
>>> Direct assigning and concatination (Ethernet + IP + TCP + HTTP)
Time diff: 0.3817322254180908s
nr = 26196 p/s
>>> Performance test pypacker vs. dpkt vs. scapy
Comparing pypacker, dpkt and scapy performance (parsing Ethernet + IP + TCP + HTTP)
nr = new results on this machine
rounds per test: 10000
>>> testing pypacker parsing speed
nr = 74695 p/s
Could not execute dpkt tests: ModuleNotFoundError("No module named 'dpkt'")
>>> testing scapy parsing speed
nr = 1732 p/s
```

# FAQ

For any questions left please file a bug (will be tagged as "questions").

**Q**:	How fast is pypacker?

**A**:	See results above. For detailed results on your machine execute tests:
	`python tests/test_pypacker.py PerfTestCase`

**Q**:	Which protocols are supported?

**A**:	Currently minimum supported protocols are:
	Ethernet, Radiotap, IEEE80211, ARP, DNS, STP, PPP, OSPF, VRRP, DTP, IP, ICMP, PIM, IGMP, IPX,
	TCP, UDP, SCTP, HTTP, NTP, RTP, DHCP, RIP, SIP, Telnet, HSRP, Diameter, SSL, TPKT, Pmap, Radius, BGP

**Q**:	Are there any plans to support [xyz]?

**A**:	New features are added to Pypacker as a result of me needing them or people contributing
	them - no formal plans for adding support for particular features in future releases exist.
	A general guideline for contribution can be found in the file HACKING.

**Q**:	How can I contribute to this project?

**A**:	Please use the Gitlab bug-tracker for bugs/feature request. Please read the bugtracker for
	already known bugs before filing a new one. Patches can be send via pull request.

**Q**:	There is problem xyz with Pypacker using Windows 3.11/XP/7/8/mobile etc. Can you fix that?

**A**:	The basic features should work with any OS. Optional ones may make trouble (eg interceptor).

**Q**:	Under which license Pypacker is issued?

**A**:	It's the GPLv2 License (see LICENSE file for more information).

**Q**:	Calling copy.deepcopy(some_packet) raises an exception "TypeError: can't pickle Struct objects".

**A**:	Try the following workaround to be able to pickle Struct objects:
```python
import struct, copyreg
def pickle_struct(s):
	return struct.Struct, (s.format,)

copyreg.pickle(struct.Struct, pickle_struct)
```


# Usage hints
## Performance related
- For maxmimum performance start accessing attributes at lowest level via the following index notation.
  This will lazy parse only needed layers behind the scene:
```
pkt_eth, pkt_ip, pkt_tcp, pkt_http = pkt[
 	None,
	(None, lambda b: b.__class__ in [ip.IP, ip6.IP6]),
	(tcp.TCP, lambda c: c.dport==80),
	http.HTTP
]
...
```

- Avoid to convert packets using the "%s" or "%r" format as it triggers parsing behind the scene:
```
pkt = Ethernet() + IP() + TCP()
# This parses ALL layers
packet_print = "%s" % pkt
```

- Avoid searching for a layer using single-value index-notation via pkt[L] as it parses all layers until L is found or highest layer is reached:
```
packet_found = pkt[Telnet]
# Alternative: Use multi-value index-notation. This will stop parsing at any non-matching layer:
packet_found = pkt[Ethernet,IP,TCP,Telnet]
```

- Use pypy (~3x faster than CPython related to full packet parsing)

- For even more performance disable auto fields (affects calling bin(...)):
```
pkt = ip.IP(src_s="1.2.3.4", dst_s="1.2.3.5") + tcp.TCP()
# Disable checksum calculation (and any other update) for IP and TCP (only THIS packet instance)
pkt.sum_au_active = False
pkt.tcp.sum_au_active = False
bts = pkt.bin(update_auto_fields=False)
```

- Enlarge receive/send buffers to get max performance. This can be done using the following commands
	(taken from: http://www.cyberciti.biz/faq/linux-tcp-tuning/):
```
sysctl -w net.core.rmem_max=12582912
sysctl -w net.core.rmem_default=12582912
sysctl -w net.core.wmem_max=12582912
sysctl -w net.core.wmem_default=12582912
sysctl -w net.core.optmem_max=2048000
sysctl -w net.core.netdev_max_backlog=5000
sysctl -w net.unix.max_dgram_qlen=1000
sysctl -w net.ipv4.tcp_rmem="10240 87380 12582912"
sysctl -w net.ipv4.tcp_wmem="10240 87380 12582912"
sysctl -w net.ipv4.tcp_mem="21228 87380 12582912"
sysctl -w net.ipv4.udp_mem="21228 87380 12582912"
sysctl -w net.ipv4.tcp_window_scaling=1
sysctl -w net.ipv4.tcp_timestamps=1
sysctl -w net.ipv4.tcp_sack=1
```

## Misc related
- Assemblation of TCP/UDP streams can be done by tshark using pipes
	with "-i -" and "-z follow,prot,mode,filter[,range]"
- Chosing the right "lowest layer" when reading capture files: Open the file eg w/ wireshark
  and look at the packet details for the data link layer. Most times this will probably
  be Ethernet II which can be parsed w/ layer12.ethernet.Ethernet.
  When capturing eg via wiresharks/tsharks "-i any" option, this will lead to Linux cooked capture
  represented by layer12.linuxcc.LinuxCC.
