# Copyright 2013, Michael Stahn
# Use of this source code is governed by a GPLv2-style license that can be
# found in the LICENSE file.
"""
Create custom Packets via keywords or from raw bytes and access/change their data
"""
from pypacker.layer3 import ip
from pypacker.layer3 import icmp

# Packet via keywords
ip0 = ip.IP(src_s="127.0.0.1", dst_s="192.168.0.1", p=1) +\
        icmp.ICMP(type=8) +\
        icmp.ICMP.Echo(id=123, seq=1, body_bytes=b"foobar")

# Packet from raw bytes. ip1_bts can also be retrieved via ip0.bin()
ip1_bts = b"E\x00\x00*\x00\x00\x00\x00@\x01;)\x7f\x00\x00\x01\xc0\xa8\x00\x01\x08\x00\xc0?\x00{\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00foobar"
ip1 = ip.IP(ip1_bts)

# Output packet (similar result for ip1)
print("%s" % ip0)
"""
layer3.ip.IP
        v_hl         (B): 0x45 = 69 = 0b1000101
        tos          (B): 0x0 = 0 = 0b0
        len          (H): 0x22 = 34 = 0b100010
        id           (H): 0x0 = 0 = 0b0
        frag_off     (H): 0x0 = 0 = 0b0
        ttl          (B): 0x40 = 64 = 0b1000000
        p            (B): 0x1 = 1 = 0b1 = IP_PROTO_ICMP
        sum          (H): 0x3B31 = 15153 = 0b11101100110001
        src          (4): b'\x7f\x00\x00\x01' = 127.0.0.1
        dst          (4): b'\xc0\xa8\x00\x01' = 192.168.0.1
        opts            : []
layer3.icmp.ICMP
        type         (B): 0x8 = 8 = 0b1000 = ICMP_ECHO
        code         (B): 0x0 = 0 = 0b0
        sum          (H): 0xC03F = 49215 = 0b1100000000111111
layer3.icmp.Echo
        id           (H): 0x7B = 123 = 0b1111011
        seq          (H): 0x1 = 1 = 0b1
        bodybytes    (6): b'foobar'
"""

# Access any header fields on any layer
ip_dst = ip1.dst_s
icmp_type = ip1.higher_layer.type

# Access layers via advanced filter (e.g. on unknown packet structure)
ip0_found, icmp0_found, echo0_found = pkt[
        (None, lambda b: b.__class__ == ip.IP),
        icmp.ICMP,
        (icmp.ICMP.Echo, lambda b: b.id == 123)
]

if echo0_found is not None:
        print(echo0_found)

# Change source IPv4 address
ip1.src_s = "1.2.3.4"

# Change ICMP payload
ip1.highest_layer.body_bytes = b"foobar2"
