# Copyright 2013, Michael Stahn
# Use of this source code is governed by a GPLv2-style license that can be
# found in the LICENSE file.
"""
Read/write packets from/to file (Support only for Wireshark/tcpdump pcap format)
"""
from pypacker import ppcap
from pypacker.layer12 import ethernet
from pypacker.layer3 import ip, ip6
from pypacker.layer4 import tcp
from pypacker.layer567 import http

preader = ppcap.Reader(filename="ether.pcap")
pwriter = ppcap.Writer(filename="ether_new.pcap", linktype=ppcap.DLT_EN10MB)

for ts, buf in preader:
        pkt = ethernet.Ethernet(buf)

        # Filter specific packets
        eth0, ip0, tcp0, http0 = pkt[
                None,
                (None, lambda b: b.__class__ in [ip.IP, ip6.IP6]),
                (tcp.TCP, lambda c: c.dport==80),
                http.HTTP
        ]

        if eth0 is not None:
                print(f"{ts}: {ip0.src_s}:{tcp0.sport} -> {ip0.dst_s}:{tcp0.dport}")
                pwriter.write(eth0.bin())

pwriter.close()
